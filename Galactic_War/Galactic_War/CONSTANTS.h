#pragma once
#include <string>
//player
int PLAYER_POSITION_X = 350;
int PLAYER_POSITION_Y = 550;
int PLAYER_LIFE_SPRITE_SIZE = 32;
int  PLAYER_SHOOT_BULLET_POSITION_DIVIDER =  3;
std::string PLAYER_LIFE_IMAGE_PATH = "image/life.png";
int KEYBOARD_SPEED_ANSWER = 7;
int PLAYER_SHOOT_TIME_CYCLE = 50;

//bullet
std::string BULLET_IMAGE_PATH = "image/bullet.png";
int BULLET_DIRECTION_Y_UP  = (-1);
int BULLET_DEFAULT_SPEED = 5;


//button
 //string DEFAULT_BUTTON_UP = "image/button_up.png";
 //string DEFAULT_BUTTON_DOWN = "image/button_down.png";