
#pragma once
#include "enemie.h"
#include "player.h"
#include "Explosion.h"
#include "bullet.h"
#include "E1.h"

class Level
{
public:
	Level(int nE1, int numberE2, int numberE3, int numberE4, int numberE5, sf::RenderWindow* w);
	void draw();
	void generate();
	void collision(Bullet* b);
	void drawListBullet();
	void drawListEnemies();
	void drawListExplosion();
	void drawBorder();
	void removePlayerLife();
	void generateBorder();
private:
	vector<Enemie*> listEnemies;
	vector<Bullet*> listBullets;
	vector<Explosion*> listExplosion;
	vector<GalacticObject* > border;
	sf::Clock timerEnemiesGeneration;
	int numberE1 = 0;
	sf::RenderWindow* window;
	Player * player;

};