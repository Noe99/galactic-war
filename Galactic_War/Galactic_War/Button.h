#pragma once
#include "GalacticObject.h"

using namespace std;

class Button : public GalacticObject
{
public:
	Button(sf::RenderWindow* window);
	Button(sf::RenderWindow* window, string path_up, string path_down);
	void draw();
	void setPosition(sf::Vector2f position);
	bool checkMousePosition();
	void switchTextureButton();
	bool isClick;
private:
	sf::RenderWindow* _window;
	string button_up;
	string button_down;
};