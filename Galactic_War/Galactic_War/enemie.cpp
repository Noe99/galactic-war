#include "enemie.h"

Enemie::Enemie()
{
}

Enemie::Enemie(string path, sf::RenderWindow* window, sf::Vector2f position, vector<Bullet*>* listB,vector<Explosion*>* listExplosion)
{
	texture.loadFromFile(path);
	sprite.setTexture(texture);
	sprite.setPosition(position);
	_window = window;
	listBullet = listB;
	explosionList = listExplosion;
}

void Enemie::draw()
{
	shoot();
	checkLife();
	_window->draw(sprite);
}

void Enemie::shoot()
{
	if (time_shoot.getElapsedTime().asMilliseconds() >= 10000)
	{
		sf::Vector2f p = sprite.getPosition();
		p.x += sprite.getTextureRect().height / 4;
		p.y += 50;
		Bullet* bullet = new Bullet("image/Ebullet.png", p, 1, 20);
		listBullet->push_back(bullet);
		int i = 0;
		time_shoot.restart();
	}
}

void Enemie::checkLife()
{
	if (sprite.getPosition().y + 50 > _window->getSize().y)
	{
		destroy = true;
		limitMap = true;
	}
	if (life <= 0) {
  		destroy = true;
		Explosion* explosion = new Explosion("image/explosion1.png", 640, 512, 128, 128);
		explosion->sprite.setPosition(sprite.getPosition().x - texture.getSize().x , sprite.getPosition().y - texture.getSize().y);
		explosionList->push_back(explosion);
	}
}
