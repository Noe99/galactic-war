#pragma once
#include "GalacticObject.h"
#include "bullet.h"
using namespace std;
class Player : public GalacticObject
{
public:
	Player(string path, sf::RenderWindow* window, vector<Bullet*>* listB);
	void shoot();
	void looseLife();
	void addLife();
	void draw();
	void clavier();
	void drawlifeBar();

private:
	sf::Texture  textureLife;
	vector<sf::Sprite> lifeBar;
	sf::Clock temps_clavier, time_shoot;
	int life = 10;
	sf::RenderWindow*  _window;
	sf::Vector2f position;
	vector<Bullet*>* listBullet;
};
