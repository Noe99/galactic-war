#include "Explosion.h"

Explosion::Explosion(string path, int imageWidth, int imageHight, int spriteWidth, int spriteHight)
{
	texture.loadFromFile(path);
	sprite.setTexture(texture);
	imgHight = imageHight;
	imgWidth = imageWidth;
	sprHight = spriteHight;
	sprWidth = spriteWidth;
	division.x = imageWidth / spriteWidth;
	division.y = imageHight / spriteHight;
	sprite.setTextureRect(sf::IntRect(sprtiteFrame.x * sprWidth, sprtiteFrame.y * sprHight, sprWidth, sprHight));
}

void Explosion::draw()
{
	if (animation_clock.getElapsedTime().asMilliseconds() > 50 && !destroy)
		    {
		        if (sprtiteFrame.y >= division.y) {
					destroy = true; 
				}
				else if (sprtiteFrame.x >= division.x)
				{
					sprtiteFrame.x = 0;
					sprtiteFrame.y++;
				}
				if (!destroy) {
					sprite.setTextureRect(sf::IntRect(sprtiteFrame.x * sprWidth, sprtiteFrame.y * sprHight, sprWidth, sprHight));
					sprtiteFrame.x++;
					animation_clock.restart();
				}
		    }
}
