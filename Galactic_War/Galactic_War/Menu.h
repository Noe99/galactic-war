#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>
#include "GalacticObject.h"
#include "Button.h"
//#include "CONSTANTS.h"
using namespace std;
class Menu : public GalacticObject
{
public:
	Menu(sf::RenderWindow* window);
	void draw();
	bool start();
private:
	sf::RenderWindow* _window;
	Button * playButton;
};