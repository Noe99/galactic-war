#ifndef NIVEAUX_H_INCLUDED
#define NIVEAUX_H_INCLUDED
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Entit�.h"
#include "fonctions.h"
#include <iostream>
#include <list>
#include <sstream>
void niveau(int x,std::string fond)
{
    /////////////////////////////////////////////////////////////////////////
///implentation du nombre de vaisseaux � tuer
    int nombre_mort = 0;
    int pourFonctionTouche = 0;
    std::stringstream test, combienEnnemi;
    test << nombre_mort;
    combienEnnemi << x;
    std::string newString = test.str();
    sf::Font MyFont;
    MyFont.loadFromFile("pixeled.ttf");
    sf::Text mort, mortNecessaire;
    mort.setFont(MyFont);
    mortNecessaire.setFont(MyFont);
    mort.setString(newString);
    mortNecessaire.setString(combienEnnemi.str());
    mort.setPosition(10, 10);
    mortNecessaire.setPosition(70, 10);
    mort.setColor(sf::Color::Red);

    sf::SoundBuffer exploBuffer, tireBuffer, mortBuffer;
    sf::Sound exploSound, tireSound, mortSound;
    sf::Music musiqueDeFond;
    music("Steamtech-Mayhem.wav",musiqueDeFond,true);

    soundEffect("explosion_3.wav", exploBuffer, exploSound, 50 );
    soundEffect("shot_2.wav", tireBuffer, tireSound, 10);
    soundEffect("lose.wav", mortBuffer, mortSound, 80);
///fond ...
    sf::Texture tfond;
    sf::Sprite sfond;
    tfond.loadFromFile(fond);
    sfond.setTexture(tfond);

///test
///test

///important
int nombre_missile = 0;
sf::RenderWindow window;
sf::Clock tempsEntreMissile, tempsDeplacement;
window.create(sf::VideoMode(600, 600), "GalacticWar");
///missiles
sf::Texture tmissile1, tmissile2, tennemi1, textureMissile;
tmissile1.loadFromFile("missile.png");
tmissile2.loadFromFile("redLaser.png");
textureMissile.loadFromFile("missileEnnemi.png");
std::vector<Missile*> missile;
std::vector<Missile*> missile_Ennemi;

///joueur
Joueur joueur;
joueur.texture_joueur.loadFromFile("moi.png");
assemblage(joueur.sprite_joueur, joueur.texture_joueur, 300,500);
///Ennemis
Ennemi ennemi[x];
tennemi1.loadFromFile("vaisseau.png");
int j = 0;
int v = 0;
int k = 100;
for (int i =0; i<x ; i++)
{
   ennemi[i].sprite_ennemi.setTexture(tennemi1);
   ennemi[i].sprite_ennemi.setPosition(10+j, 40+v);
   ennemi[i].missile_Ennemi.sprite_missile.setTexture(textureMissile);
   j=j+k;
   if (j > 900)
   {
       k= k + 100;
       j= 0;
       v= v + 100;
   }

   ennemi[i].x = 0;
   ennemi[i].y = 0;
}
/////////////////////////////////////////////////////////////////////////////
while (window.isOpen())
    {

        sf::Event event1;





while (window.pollEvent(event1))
        {
            if(nombre_mort == x)
            {
                window.close();
            }
        }

    mort.setString(test.str());
    test.str("");
    test << nombre_mort;
///commande clavier
        clavier(joueur.sprite_joueur, joueur.temps_clavier);
///cr�er autant de missile que le joueur veut...
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
        {
            if (tempsEntreMissile.getElapsedTime().asMilliseconds()> 600)
            {
                tireSound.play();
                Missile * m =new Missile();
          m->sprite_missile.setTexture(tmissile1);
          m->sprite_missile.setPosition(joueur.sprite_joueur.getPosition().x,joueur.sprite_joueur.getPosition().y);
          m->sprite_missile.move(0,-1);
          missile.push_back(m);
          tempsEntreMissile.restart();
            }
        }
       /* if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        {
            if (tempsEntreMissile.getElapsedTime().asMilliseconds()> 1000)
            {
                Missile * m =new Missile();
          m->sprite_missile.setTexture(tmissile2);
          m->sprite_missile.setPosition(joueur.sprite_joueur.getPosition().x,joueur.sprite_joueur.getPosition().y-250);
          m->sprite_missile.move(0,-1);
          missile.push_back(m);
          tempsEntreMissile.restart();
            }
        }*/

    window.clear();
    window.draw(sfond);
///anime les ennemis ...
        for (int i =0; i<x ; i++)
{
    annimation(ennemi[i].sprite_ennemi, ennemi[i].x, ennemi[i].y, 46, 60, ennemi[i].ping );
    deplacement(ennemi[i].direction,ennemi[i].sprite_ennemi,ennemi[i].deplacementEnnemi );
    if (ennemi[i].missile_Ennemi.temps_missile.getElapsedTime().asSeconds()> rand())
            {

                Missile * mm =new Missile();
          mm->sprite_missile.setTexture(textureMissile);
          mm->sprite_missile.setPosition(ennemi[i].sprite_ennemi.getPosition().x,ennemi[i].sprite_ennemi.getPosition().y);
          missile_Ennemi.push_back(mm);
          ennemi[i].missile_Ennemi.temps_missile.restart();
            }

}
///dessine les missiles ...
        for (int i = 0 ; i < missile.size() ; i++ )
{
    window.draw(missile[i]->sprite_missile);

    missile[i]->sprite_missile.move(0,-0.4);
    /// si un missile touche un ennemi ennemis disparait...
    for (int u =0; u<x ; u++)
      {
         touche(46, 60, missile[i]->sprite_missile, ennemi[u].sprite_ennemi, nombre_mort, exploSound );

      }
}
 for (int i = 0 ; i < missile_Ennemi.size() ; i++ )
{
    window.draw(missile_Ennemi[i]->sprite_missile);
    missile_Ennemi[i]->sprite_missile.move(0,0.2);
    touche(30,30, missile_Ennemi[i]->sprite_missile, joueur.sprite_joueur, pourFonctionTouche , mortSound);
}

///dessine les ennemis...
for (int i =0; i<x ; i++)
{
    window.draw(ennemi[i].sprite_ennemi);
}

///dessine le joueur...
    window.draw(joueur.sprite_joueur);
///comteur de mort...
    window.draw(mort);
    window.draw(mortNecessaire);
    window.display();

    }
}


void boss1(std::string fond)
{
    ///fond...
    sf::Texture tfond;
    sf::Sprite sfond;
    tfond.loadFromFile(fond);
    sfond.setTexture(tfond);


    ///joueur...
    Joueur joueur;
    joueur.texture_joueur.loadFromFile("moi.png");
    assemblage(joueur.sprite_joueur, joueur.texture_joueur, 300,500);

    ///missile joueur...
    sf::Texture tmissile1;
    tmissile1.loadFromFile("missile.png");
    std::vector<Missile*> missile;
    sf::Clock tempsEntreMissile, tempsDeplacement, tempsEntreMissilleBoss;

    /// missile boss...
    sf::Texture tmissileBoss;
    std::vector<Missile*> missileBoss;
    tmissileBoss.loadFromFile("missileBoss.png");

    ///boss
    Boss boss;
    boss.textureBoss.loadFromFile("boss1.png");
    assemblage(boss.spriteBoss, boss.textureBoss, 150,150);

    ///fenetre ...
    sf::RenderWindow window;
    window.create(sf::VideoMode(600, 600), "GalacticWar");
//////////////////////////////////////////////////////////////////////////////////////////
    while (window.isOpen())
{
 window.clear();
 window.draw(sfond);
 clavier(joueur.sprite_joueur, joueur.temps_clavier);
 deplacement(boss.deplace, boss.spriteBoss, boss.deplacementbossTemps);


    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
        {
            if (tempsEntreMissile.getElapsedTime().asMilliseconds()> 600)
            {

                Missile * m =new Missile();
          m->sprite_missile.setTexture(tmissile1);
          m->sprite_missile.setPosition(joueur.sprite_joueur.getPosition().x,joueur.sprite_joueur.getPosition().y);
          m->sprite_missile.move(0,-1);
          missile.push_back(m);
          tempsEntreMissile.restart();
            }
        }

    if (tempsEntreMissilleBoss.getElapsedTime().asMilliseconds()> rand())
            {

                Missile * m =new Missile();
                Missile * mm =new Missile();

          m->sprite_missile.setTexture(tmissileBoss);
          mm->sprite_missile.setTexture(tmissileBoss);

          m->sprite_missile.setPosition(boss.spriteBoss.getPosition().x+20,boss.spriteBoss.getPosition().y);
          mm->sprite_missile.setPosition(boss.spriteBoss.getPosition().x+130,boss.spriteBoss.getPosition().y);

          missileBoss.push_back(m);
          missileBoss.push_back(mm);
          tempsEntreMissilleBoss.restart();
            }


  for (int i = 0 ; i < missile.size() ; i++ )
{
    window.draw(missile[i]->sprite_missile);
    missile[i]->sprite_missile.move(0,-0.4);
}
for (int i = 0 ; i < missileBoss.size() ; i++ )
{
    window.draw(missileBoss[i]->sprite_missile);
    missileBoss[i]->sprite_missile.move(0,0.4);
}

 window.draw(joueur.sprite_joueur);
 window.draw(boss.spriteBoss);
 window.display();
}
}






/////////////////////////////////////////////////////////
///
///
///
///
void Menu()
{
    float walkingSpeed (1.0);
    sf::Clock walkingClock, animationClock;

    ///Cr�ation de la fen�tre
    sf::RenderWindow window(sf::VideoMode(800, 800), "Menu");
    Bouton_play play;
    Bouton_quit quit ;

    ///Cr�ation des textures et sprites
    sf::Texture t_play1, t_play2, t_fond, t_quit1, t_quit2, character;
    sf::Sprite sprite_play1, sprite_play2, sprite_quit1, sprite_quit2, sprite_fond, character_sprite;

    ///Assemblage des sprites (Fond, boutons play et quit, titre, commandes, personnage)
    t_fond.loadFromFile("Fond_menu.png");
    sprite_fond.setTexture(t_fond);
    sprite_fond.setPosition(sf::Vector2f(0,0));

    t_play1.loadFromFile("PLAY_1.png");
    play.sprite_play1.setTexture(t_play1);
    play.sprite_play1.setPosition(304, 210);

    t_play2.loadFromFile("PLAY_2.png");
    play.sprite_play2.setTexture(t_play2);
    play.sprite_play2.setPosition(304, 210);

    t_quit1.loadFromFile("QUIT_1.png");
    quit.sprite_quit1.setTexture(t_quit1);
    quit.sprite_quit1.setPosition(304, 290);

    t_quit2.loadFromFile("QUIT_2.png");
    quit.sprite_quit2.setTexture(t_quit2);
    quit.sprite_quit2.setPosition(304, 290);

    sf::Font font_titre;
    font_titre.loadFromFile("ARCADECLASSIC.TTF");

    sf::Text titre;
    titre.setFont(font_titre);
    titre.setString("WarShips");
    titre.setFillColor(sf::Color::Black);
    titre.setCharacterSize(70);
    titre.setPosition(250, 50);

    sf::Text commandes;
    commandes.setFont(font_titre);
    commandes.setString("Utilisez  ZQSD  pour  deplacer  votre  personnage  ! ");
    commandes.setFillColor(sf::Color::Black);
    commandes.setCharacterSize(30);
    commandes.setPosition(22, 612);

    character.loadFromFile("hero.png");
    character_sprite.setTexture(character);
    character_sprite.setPosition(0, 670);
    int x (0), y(0);

    ///Liste des �v�nements s� d�roulant pendant que la fen�tre est ouverte
    while (window.isOpen())
    {

        sf::Event event;
        while (window.pollEvent(event))
        {
            ///La fen�tre se ferme si on appuie sur la croix
            if (event.type == sf::Event::Closed)
              {
                window.close();
              }
        }

        if (walkingClock.getElapsedTime().asMilliseconds()>15)
        {
            /// D�placements verticaux et horizontaux
           if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z)) ///Haut
            {
                y = 3;
                character_sprite.move(0, -walkingSpeed);
                walkingClock.restart();
            }

            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) ///Gauche
            {
                y =1;
                character_sprite.move(-walkingSpeed, 0);
                walkingClock.restart();
            }

            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)) ///Bas
            {
                y = 0;
                character_sprite.move(0, walkingSpeed);
                walkingClock.restart();
            }

            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))  ///Droite
            {
                y = 2;
                character_sprite.move(walkingSpeed, 0);
                walkingClock.restart();
            }

            else ///Si aucune touche n'est appuy�e, l'animation s'arr�te
            {
                x = 0;
            }

            /// D�placements diagonaux
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) & sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
            {
                character_sprite.move(-walkingSpeed/2, walkingSpeed/2);
            }

            else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) & sf::Keyboard::isKeyPressed(sf::Keyboard::D))
            {
                character_sprite.move(walkingSpeed/2, walkingSpeed/2);
            }

            else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z) & sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
            {
                character_sprite.move(-walkingSpeed/2, -walkingSpeed/2);
            }

            else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z) & sf::Keyboard::isKeyPressed(sf::Keyboard::D))
            {
                character_sprite.move(walkingSpeed/2, -walkingSpeed/2);
            }
            /////////////////////////////////////////////////////////////////////////

            if (animationClock.getElapsedTime().asMilliseconds()>50) ///temps d'ex�cution de l'animation
            {
                annimation(character_sprite, x, y);
                animationClock.restart();
            }

            /// Sprint
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
            {
                walkingSpeed = 2.0;
            }

            else
            {
                walkingSpeed = 1.0;
            }
            //////////////////////////////////////////////////////

            walkingClock.restart();

            /// Le personnage ne sortira pas de l'�cran ///

             if (character_sprite.getPosition().x <= 0)
            {
                character_sprite.setPosition(sf::Vector2f(0, character_sprite.getPosition().y));
            }

            if (character_sprite.getPosition().x >= 770)
            {
                character_sprite.setPosition(sf::Vector2f(770, character_sprite.getPosition().y));
            }

            if (character_sprite.getPosition().y <= 630)
            {
                character_sprite.setPosition(sf::Vector2f(character_sprite.getPosition().x, 630));
            }

            if (character_sprite.getPosition().y >= 710)
            {
                character_sprite.setPosition(sf::Vector2f(character_sprite.getPosition().x, 710));
            }
        }

        window.clear();
        window.draw(sprite_fond);
        window.draw(play.sprite_play1);
        window.draw(quit.sprite_quit1);
        window.draw(character_sprite);
        window.draw(titre);
        window.draw(commandes);

        ///Si on appuie sur Play, le jeu se lance
         if ((std::abs((sf::Mouse::getPosition(window).x)-(play.sprite_play1.getPosition().x+89))< 64)&&(std::abs((sf::Mouse::getPosition().y+17)-(play.sprite_play1.getPosition().y+290))< 30))
            {
                window.draw(play.sprite_play2);
                if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
                    {
                        window.close();
                    }
            }

        ///Si on appuie sur Quit, la fen�tre se ferme
        if ((std::abs((sf::Mouse::getPosition(window).x)-(quit.sprite_quit1.getPosition().x+89))< 64)&&(std::abs((sf::Mouse::getPosition().y+17)-(quit.sprite_quit1.getPosition().y+290))< 30))
            {
                window.draw(quit.sprite_quit2);
                if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
                    {
                        window.close();
                    }
            }


        window.display();
    }


}

#endif // NIVEAUX_H_INCLUDED
