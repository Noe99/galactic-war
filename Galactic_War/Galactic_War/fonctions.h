#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Entit�.h"
#include "Niveaux.h"
#include <iostream>
#include <sstream>
///permet l'assemblage texture + sprite = image sur la fenetre...
void assemblage(sf::Sprite&  s,sf::Texture& t,int x, int y)
    {
        s.setTexture(t);
        s.setPosition(x,y);
    }

///fonction de deplacement ennemis
void deplacement(bool & direction, sf::Sprite & ennemi, sf::Clock &tempsDeplacementEnnemi)
 {
  if (tempsDeplacementEnnemi.getElapsedTime().asMilliseconds()>8)
  {
    if(direction == false)
            {
               ennemi.move(-1, 0);
            }
    if(direction == true)
            {
               ennemi.move(1, 0);
            }

    if(ennemi.getPosition().x>= 800)
    {
        direction = false;
    }

    if(ennemi.getPosition().x<= -200)
    {
        direction = true;
    }
   tempsDeplacementEnnemi.restart();
  }
 }

///fonction annimation
void annimation ( sf::Sprite & s, int& x, int& y, int xx, int yy, sf::Clock & c)
{
if (c.getElapsedTime().asMilliseconds()> 100)
{
     if (x >= 4)
    {
        x=0;

    }

        s.setTextureRect(sf::IntRect(x*xx, y * yy, xx, yy));
        x++;

    c.restart();
}

}


///definition des touche clavi� et de leur fonctions...
 void clavier(sf::Sprite & s, sf::Clock  &c)
{
    int i = 1;

if (c.getElapsedTime().asMilliseconds() >= 7)
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
        {
             s.move(i, 0);

        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
        {
             s.move(-i, 0);

        }

        c.restart();
    }
if (s.getPosition().x <= 0)
{
    s.setPosition(sf::Vector2f(0, s.getPosition().y));
}
if (s.getPosition().x >= 550)
{
    s.setPosition(sf::Vector2f(550, s.getPosition().y));
}
}

/// missile touche ennemis
void touche(int taille_X, int taille_Y, sf::Sprite & s1, sf::Sprite & s2 , int & nombre_ennemis_tue, sf::Sound & sound)
{
    if ((std::abs((s1.getPosition().x+(taille_X/2))-(s2.getPosition().x+(taille_X/2)))< taille_X)&&(std::abs((s1.getPosition().y+(taille_Y/2))-(s2.getPosition().y+(taille_Y/2)))< taille_Y))
    {
        s2.setPosition(8000, 8000);
        s1.setPosition(8000, 0);
        nombre_ennemis_tue = nombre_ennemis_tue + 1 ;
        sound.play();


    }
}

///tire ennemis
void tireEnnemi(sf::Sprite & ennemi , sf::Sprite& missile)
{

}


///son musique

void music(std::string s, sf::Music & m, bool loop)
{
m.openFromFile(s);
m.play();
m.setLoop(loop);
m.setVolume(100);
}


///son autres

void soundEffect (std::string soundName, sf::SoundBuffer & sBuffer, sf::Sound & s, int volume)
{
    sBuffer.loadFromFile(soundName);
    s.setBuffer(sBuffer);
    s.setVolume(volume);
}


///Tout pour le menu ///
void annimation ( sf::Sprite& s, int& x , int& y)
{
    if (x>3)
    {
        x=0;
    }

    s.setTextureRect(sf::IntRect(x*32, y * 48, 32, 48));
    x++;
}
///


#endif // FONCTIONS_H_INCLUDED
