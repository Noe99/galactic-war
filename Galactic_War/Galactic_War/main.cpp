
#include <cmath>
//#include <SFML/Graphics.hpp>
//#include <SFML/Audio.hpp>
#include <time.h>
#include "Menu.h"
#include "level.h"

int main()
{
    srand(time(NULL));
    sf::RenderWindow window(sf::VideoMode(700, 700), "SFML works!");
    Level l1(50,0,0,0,0, &window);
    Menu menu(&window);
    ///cr�ation d'une fenetre
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        if (!menu.start()) {
            menu.draw();
        }
        else {
            l1.draw();
        }
        window.display();
    }

    return 0;
}

