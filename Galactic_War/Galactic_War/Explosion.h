#pragma once
#include "GalacticObject.h"
using namespace std;

class Explosion : public GalacticObject
{
public:
	Explosion(string path, int imageWidth, int imageHight, int spriteWidth, int spriteHight);
	void draw();
	bool destroy = false;

private:
	int imgWidth;
	int imgHight;
	int sprWidth;
	int sprHight;
	sf::Clock  animation_clock;
	sf::Vector2i sprtiteFrame = sf::Vector2i(0, 0);
	sf::Vector2i division;
};