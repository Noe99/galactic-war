#pragma once
#include "Explosion.h"
#include "bullet.h"
class Enemie : public GalacticObject{
public:
	Enemie();
	Enemie(string path, sf::RenderWindow* window, sf::Vector2f position, vector<Bullet*>* listBullet, vector<Explosion*>* listExplosion);
	virtual void draw();
	void shoot();
	//virtual void animation() = 0;
	void checkLife(); 
	bool destroy = false;
	bool limitMap = false;

protected :
	sf::RenderWindow* _window;
	sf::Clock time_shoot, animation_clock, move_clock;
	sf::Vector2i sprtiteFrame = sf::Vector2i(0,0);
	vector<Bullet*> * listBullet;
	vector<Explosion*> * explosionList;
};