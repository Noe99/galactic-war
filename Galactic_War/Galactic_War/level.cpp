#include "level.h"

Level::Level(int nE1, int numberE2, int numberE3, int numberE4, int numberE5, sf::RenderWindow* w)
{
	player = new Player("image/player.png", w, &listBullets);
	window = w;
	numberE1 = nE1;
	generateBorder();
}

void Level::draw()
{
	drawListBullet();
	player->draw();
	generate();
	drawListEnemies();
	drawListExplosion();
	drawBorder();
}

void Level::generate()
{
	if (timerEnemiesGeneration.getElapsedTime().asMilliseconds() >= 2000 && numberE1 > 0)
	{

		numberE1--;
		sf::Vector2f position;
		position.x =  rand() % window->getSize().x - player->sprite.getTexture()->getSize().x;
		position.y = 0;
		E1* e = new E1(window, position, &listBullets, &listExplosion);
		listEnemies.push_back(e);

		timerEnemiesGeneration.restart();
	}
}

void Level::collision(Bullet* b)
{
	for (Enemie* e : listEnemies) {

		sf::Vector2f widthEnemie = sf::Vector2f(e->sprite.getPosition().x, e->sprite.getTextureRect().width + e->sprite.getPosition().x);
		sf::Vector2f heightEnemie = sf::Vector2f(e->sprite.getPosition().y, e->sprite.getTextureRect().height + e->sprite.getPosition().y);

		if ((b->sprite.getPosition().x + 0.5*b->sprite.getTextureRect().width >= e->sprite.getPosition().x) &&
			(b->sprite.getPosition().x + 0.5 * b->sprite.getTextureRect().width <= e->sprite.getTextureRect().width + e->sprite.getPosition().x) &&
			(b->sprite.getPosition().y >= e->sprite.getPosition().y) &&
			(b->sprite.getPosition().y <= e->sprite.getTextureRect().height + e->sprite.getPosition().y))
		{
			b->destroy = true;
   			e->life--;
		}

	}
}

void Level::drawListBullet()
{
	int i = 0;
	vector<int> erase;
	for (Bullet* b : listBullets) {
		collision(b);
		if (b->destroy) {
			erase.push_back(i);
		}
		else {
			b->draw(window);
		}
		i++;
	}

	for (int i : erase) {
		Bullet* b = listBullets[i];
		listBullets.erase(listBullets.begin() + i);
		delete b;
	}
}

void Level::drawListEnemies()
{
	int i = 0;
	vector<int> erase;
	for (Enemie* e : listEnemies) {
		if (e->destroy) {
			if (e->limitMap) {
				removePlayerLife();
			}
			erase.push_back(i);
		}
		else {
			e->draw();
		}
		i++;
	}

	for (int i : erase) {
		Enemie* e = listEnemies[i];
		listEnemies.erase(listEnemies.begin() + i);
		delete e;
	}
}

void Level::drawListExplosion()
{
	int i = 0;
	vector<int> erase;
	for (Explosion* e : listExplosion) {
		if (e->destroy) {
			erase.push_back(i);
		}
		else {
			e->draw();
			window->draw(e->sprite);
		}
		i++;
	}

	for (int i : erase) {
		Explosion* e = listExplosion[i];
		listExplosion.erase(listExplosion.begin() + i);
		delete e;
	}
}

void Level::drawBorder()
{
	for (GalacticObject* tile : border) {
		window->draw(tile->sprite);
	}
}

void Level::removePlayerLife()
{
	player->looseLife();
}

void Level::generateBorder()
{
	int width = window->getSize().x / 16;
	int hight = window->getSize().y / 16;
	for (int i = 0; i <= hight; i++) {
		for (int j = 0; j <= width; j++) {
			if (j == 0 || j == width || i == 0 || i == hight) {
				GalacticObject* tile = new GalacticObject;
				tile->texture.loadFromFile("image/border.png");
				tile->sprite.setTexture(tile->texture);
				tile->sprite.setPosition(j * 15.9, i * 15.9);
				border.push_back(tile);
			}
		}
	}
}

