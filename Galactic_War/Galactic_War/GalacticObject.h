#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>
#include <string>
#include <vector>

struct GalacticObject
{
	sf::Texture texture;
	sf::Sprite sprite;
	int life = 0;
};
