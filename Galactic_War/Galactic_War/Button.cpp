#include "Button.h"

Button::Button(sf::RenderWindow* window)
{
	texture.loadFromFile("image/button_up.png");
	sprite.setTexture(texture);
	_window = window;
	button_down = "image/button_down.png";
	button_up = "image/button_up.png";
}

Button::Button(sf::RenderWindow* window, string path_up, string path_down)
{
	texture.loadFromFile(path_up);
	sprite.setTexture(texture);
	_window = window;
	button_down = path_down;
	button_up = path_up;
}

void Button::draw()
{
	switchTextureButton();
	_window->draw(sprite);
}

void Button::setPosition(sf::Vector2f position)
{
	sprite.setPosition(position);
}

bool Button::checkMousePosition()
{
	sf::Vector2i position = sf::Mouse::getPosition(*_window);
	if(position.x >= sprite.getPosition().x &&
		position.x <= sprite.getPosition().x + texture.getSize().x &&
		position.y >= sprite.getPosition().y &&
		position.y <= sprite.getPosition().y + texture.getSize().y)
	{ 
		return true;
	}

	return false;
}

void Button::switchTextureButton()
{
	if (checkMousePosition()) {
		texture.loadFromFile(button_down);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
			isClick = true;
		}
	}
	else {
		texture.loadFromFile(button_up);
	}
	sprite.setTexture(texture);

}
