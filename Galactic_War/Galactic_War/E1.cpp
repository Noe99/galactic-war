#include "E1.h"

E1::E1(sf::RenderWindow* window, sf::Vector2f position, vector<Bullet*>* listB, vector<Explosion*>* listExplosion)
{
	texture.loadFromFile(path);
	sprite.setTexture(texture);
    sprite.setPosition(position);
	_window = window;
	life = 2;
	listBullet = listB;
	explosionList = listExplosion;
}

//void E1::animation()
//{
//    if (animation_clock.getElapsedTime().asMilliseconds() > 200)
//    {
//        if (sprtiteFrame.x >= 4)
//        {
//            sprtiteFrame.x = 0;
//            sprtiteFrame.y++;
//        }
//        else if (sprtiteFrame.x >= 2) {
//            sprtiteFrame.y = 0;
//        }
//        sprite.setTextureRect(sf::IntRect(sprtiteFrame.x * 47, sprtiteFrame.y * 63, 47, 63));
//        sprtiteFrame.x++;
//        animation_clock.restart();
//    }
//}

void E1::draw()
{
    //animation();
	shoot();
	checkLife();
	move();
    _window->draw(sprite);
}

void E1::move()
{
	if (move_clock.getElapsedTime().asMilliseconds() > 40)
		{
			sprite.move(0, 1);
			move_clock.restart();
		}
}


