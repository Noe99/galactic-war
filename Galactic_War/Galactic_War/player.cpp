#include "player.h"

Player::Player(string path, sf::RenderWindow* window, vector<Bullet*>* listB)
{
    listBullet = listB;
	texture.loadFromFile(path);
    textureLife.loadFromFile("image/life.png");
	sprite.setTexture(texture);
	_window = window;
	position.x = 350;
	position.y = 550;
	sprite.setPosition(position);

    for (int i = 0; i < life; i++) {
        sf::Sprite lifeSprite;
        lifeSprite.setTexture(textureLife);
        lifeSprite.setPosition(sf::Vector2f(
            _window->getSize().x - 
            life * textureLife.getSize().x + 
            i* textureLife.getSize().x, 620)
        );
        lifeBar.push_back(lifeSprite);
    }
}

void Player::shoot()
{
    sf::Vector2f p = position;
    p.x += sprite.getTextureRect().width / 3;
    Bullet* bullet = new Bullet("image/bullet.png", p, -1, 5);
    listBullet->push_back(bullet);
    int i = 0;
}

void Player::looseLife()
{
	life--;
    if(lifeBar.size()> 0)
    lifeBar.pop_back();
}

void Player::addLife()
{
	life++;
}

void Player::draw()
{
    drawlifeBar();
    clavier();
	_window->draw(sprite);
}


void Player::clavier()
{
    int i = 1;

    if (temps_clavier.getElapsedTime().asMilliseconds() >= 7)
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            position = sprite.getPosition();
            sprite.move(i, 0);

        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            position = sprite.getPosition();
            sprite.move(-i, 0);

        }

        temps_clavier.restart();
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && time_shoot.getElapsedTime().asMilliseconds() >= 50)
    {
        shoot();
        time_shoot.restart();
    }
    if (sprite.getPosition().x <= 0)
    {
        sprite.setPosition(sf::Vector2f(0, sprite.getPosition().y));
    }
    if (sprite.getPosition().x >= _window->getSize().x - sprite.getTextureRect().width)
    {
        sprite.setPosition(sf::Vector2f(_window->getSize().x - sprite.getTextureRect().width, sprite.getPosition().y));
    }
}


void Player::drawlifeBar()
{
    for (sf::Sprite s : lifeBar) {
        _window->draw(s);
    }

}
