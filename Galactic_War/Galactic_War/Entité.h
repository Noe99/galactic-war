#ifndef ENTIT�_H_INCLUDED
#define ENTIT�_H_INCLUDED
#include <cmath>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Niveaux.h"
#include "fonctions.h"
#include <iostream>
/// definition du joueur

class Joueur
{
public:
    sf::Sprite sprite_joueur;
    sf::Clock temps_clavier;
    sf::Texture texture_joueur;
};





class Missile
{
    public:
    sf::Sprite sprite_missile;
    sf::Clock temps_missile;
};


class Boss
{
    public:
    sf::Sprite spriteBoss;
    sf::Clock deplacementbossTemps;

    sf::Texture textureBoss;
    int vieBoss;
    bool deplace = false;


};




class Ennemi
{
public:
    sf::Sprite sprite_ennemi, ennemi_explosion;
    sf::Clock ping, deplacementEnnemi;
    sf::Texture texture_ennemi;
    int vie_ennemi, x , y;
    bool direction = false;
    Missile missile_Ennemi;




};

///Entit� bouton

class Bouton_play
{
    public:
    sf::Sprite sprite_play1, sprite_play2;

};

class Bouton_quit
{
    public:
    sf::Sprite sprite_quit1, sprite_quit2;

};


#endif // ENTIT�_H_INCLUDED
