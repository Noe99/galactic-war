#pragma once
#include "enemie.h"
class E1 : public Enemie
{
public:
	E1(sf::RenderWindow* window, sf::Vector2f position, vector<Bullet*>* listB, vector<Explosion*>* listExplosion);
	//void animation();
	void draw();
	void move(); 
private:
	string path = "image/E1.png";
};
