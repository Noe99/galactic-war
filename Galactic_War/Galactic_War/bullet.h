#pragma once

#include "GalacticObject.h"
using namespace std;
class Bullet : public GalacticObject
{
public:
	Bullet(string path, sf::Vector2f position, int d, int speed);
	void draw(sf::RenderWindow* window);
	void advanced();
	bool destroy = false;

private:
	sf::Vector2f _position;
	sf::Clock timer;
	int direction;
	int _speed;
};

