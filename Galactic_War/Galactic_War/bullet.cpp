#include "bullet.h"

Bullet::Bullet(string path, sf::Vector2f position , int d, int speed)
{
	texture.loadFromFile(path);
	sprite.setTexture(texture);
	sprite.setPosition(position);
	_position = position;
	direction = d;
	_speed = speed;
}

void Bullet::draw(sf::RenderWindow* window)
{
	advanced();
	window->draw(sprite);
}

void Bullet::advanced()
{
	if (timer.getElapsedTime().asMilliseconds() >= _speed) {
		_position.y+= direction;
		sprite.setPosition(_position);
		if (_position.y < 0) {
			destroy = true;
		}
		timer.restart();
	}
}
