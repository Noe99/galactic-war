#include "Menu.h"

Menu::Menu(sf::RenderWindow* window)
{
	texture.loadFromFile("image/menu.jpg");
	sprite.setTexture(texture);
	_window = window;
	playButton = new Button(window);
	playButton->setPosition(sf::Vector2f(350, 350));
}

void Menu::draw()
{
	_window->draw(sprite);
	playButton->draw();
}

bool Menu::start()
{
	if (playButton->isClick) {
		return true;
	}
	return false;
}
